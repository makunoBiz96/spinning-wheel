let previousDegree = 0;
let factor = 10;
let degree = 0;
let realDegree = 23;

function spin() {
	degree = randomDeg();
	factor = randomFactor();

	// var deg = Math.floor(Math.random() * (x-y)) + y;
	let deg = previousDegree + (degree * factor);
	realDegree += deg;

	document.getElementById('box').style.transform = "rotate(" + deg + "deg)";

	let element = document.getElementById('mainbox');
	element.classList.remove('animate');
	element.classList.add('animate');

	setTimeout(function () {
		element.classList.add('animate');
		getLocation();
	}, 5000);

	previousDegree = deg;
}

function randomDeg() {
	let x = 1; //min value
	let y = 360; //max value
	return Math.floor(Math.random() * (y - x));
}

function randomFactor() {
	let x = 10; //min value
	if (degree < 150) {
		x = 10;
	}
	if (degree < 100) {
		x = 15;
	}
	let y = 24; //max value
	return Math.floor(Math.random() * (y - x) + x);
}

/**
 * The circle is divided into 8 equal pies so each pie spans between 45 degrees
 */
function getLocation() {
	let location = realDegree % 360;
	let value = null;
	if (location > 0 && location <= 45) {
		value = "D";
	}
	if (location > 45 && location <= 90) {
		value = "F";
	}
	if (location > 90 && location <= 135) {
		value = "A";
	}
	if (location > 135 && location <= 180) {
		value = "H";
	}
	if (location > 180 && location <= 225) {
		value = "C";
	}
	if (location > 225 && location <= 270) {
		value = "E";
	}
	if (location > 270 && location <= 315) {
		value = "B";
	}
	if (location > 315 && location <= 360) {
		value = "G";
	}
	// return value;
	swal({
		title: "Returned value is " + value,
		// text: "You won "+value,
		// icon: "success",
		button: "Play Again"
		// buttons: true,
		// dangerMode: true,
	}).then((value) => {
		document.location.href = "index.html"; //reset the data back to
	});
}